use crate::{
    error::Error, hover, util::{color_image_gray_alpha, create_texture_options, read_as_bti, ValidMagFilter, ValidTextureFormat}, ICON_PNG, OPEN_WITH
};
use eframe::{
    CreationContext,
    egui::{
        self, load::SizedTexture, Align, Color32, ColorImage, ImageData, ImageSource, LayerId,
        Layout, Margin, Order, Rounding, TextBuffer, Ui, Widget,
    },
    epaint::Shadow,
};
use emath::{Pos2, Rect, TSTransform};
use epaint::TextureHandle;
use gczen::{
    companion::bti::BtiCompanion,
    gc::{
        bti::{Bti, Header},
        types::bti::{
            Anisotropy, JUTTransparency, PaletteFormat, TextureFilter, TextureWrap,
        },
    },
};
use image::{ColorType, ImageFormat};
use std::{
    fs,
    io::{BufRead, BufReader, Cursor, Seek},
    path::PathBuf,
    sync::Arc,
};

const COL_WIDTH: f32 = 250.0 * 0.5;

pub struct MainApp {
    transform: TSTransform,
    path: PathBuf,
    orig_png: Option<Vec<u8>>,
    image: Option<TextureHandle>,
    cur_header: Header,
    sel_header: Header,
    tmp_lod_bias: f32,
    tmp_min_lod: f32,
    tmp_max_lod: f32,
    last_pos: Option<Pos2>,
    first_run: bool,
    show_about: bool,
    err: String,
}

impl MainApp {
    pub fn create(cc: &CreationContext) -> Box<Self> {
        let ctx = &cc.egui_ctx;
        // This gives us image support:
        egui_extras::install_image_loaders(ctx);

        // Get current context style
        let mut style = (*ctx.style()).clone();

        style.visuals.menu_rounding = Rounding::same(1.0);
        style.visuals.window_rounding = Rounding::same(1.0);
        style.visuals.window_shadow = Shadow::NONE;
        style.visuals.popup_shadow = Shadow::NONE;
        style.visuals.widgets.noninteractive.bg_fill = Color32::from_gray(48);
        style.visuals.widgets.noninteractive.bg_stroke.color = Color32::from_gray(16);
        style.visuals.widgets.noninteractive.bg_stroke.width = 0.5;
        style.visuals.window_fill = Color32::from_gray(40);
        style.visuals.window_stroke.color = Color32::from_gray(10);
        style.visuals.window_stroke.width =
            style.visuals.widgets.noninteractive.bg_stroke.width;
        style.visuals.panel_fill = style.visuals.window_fill;
        style.spacing.menu_margin = Margin::same(3.0);
        style.spacing.item_spacing = egui::emath::Vec2::new(4.0, 4.0);
        style.spacing.button_padding = egui::emath::Vec2::new(5.0, 5.0);
        style.spacing.window_margin.right = 100.0;

        ctx.set_style(style);

        Box::<MainApp>::default()
    }

    pub fn process_new_file(&mut self, ctx: &egui::Context, path: PathBuf) -> Result<(), Error> {
        let Some(ext) = path.extension() else {
            return Ok(());
        };
        let png_path = path.with_extension("png");
        self.path = png_path.clone();
        let Some(filename) = png_path.file_name() else {
            return Ok(());
        };

        self.first_run = true;
        let data = fs::read(&path)?;
        let bti = read_as_bti(path.clone(), &data)?;

        let image_data = match ext.to_string_lossy().as_str() {
            "bti" => {
                self.tmp_lod_bias = bti.header.lod_bias as f32 * 0.01;
                self.tmp_min_lod = bti.header.min_lod as f32 * 0.125;
                self.tmp_max_lod = bti.header.max_lod as f32 * 0.125;
                self.sel_header = bti.header.clone();
                self.cur_header = bti.header.clone();

                let (image_type, img_data) = bti.generate_png_data()?;

                let mut png_bytes = Vec::new();
                let mut png_buf = Cursor::new(&mut png_bytes);

                image::write_buffer_with_format(
                    &mut png_buf,
                    &img_data,
                    self.cur_header.width as u32,
                    self.cur_header.height as u32,
                    image_type,
                    ImageFormat::Png,
                )?;

                self.orig_png = Some(png_bytes);

                match image_type {
                    ColorType::L8 => ImageData::Color(Arc::new(ColorImage::from_gray(
                        [
                            self.sel_header.width as usize,
                            self.sel_header.height as usize,
                        ],
                        &img_data,
                    ))),
                    ColorType::La8 => ImageData::Color(Arc::new(color_image_gray_alpha(
                        [
                            self.sel_header.width as usize,
                            self.sel_header.height as usize,
                        ],
                        &img_data,
                    ))),
                    ColorType::Rgb8 => ImageData::Color(Arc::new(ColorImage::from_rgb(
                        [
                            self.sel_header.width as usize,
                            self.sel_header.height as usize,
                        ],
                        &img_data,
                    ))),
                    ColorType::Rgba8 => {
                        ImageData::Color(Arc::new(ColorImage::from_rgba_unmultiplied(
                            [
                                self.sel_header.width as usize,
                                self.sel_header.height as usize,
                            ],
                            &img_data,
                        )))
                    }
                    _ => unreachable!("somehow weird unsupported image type was found!"),
                }
            }
            "png" => {
                self.orig_png = Some(data.clone());
                let bytes = BufReader::new(Cursor::new(data.clone()));

                self.tmp_lod_bias = bti.header.lod_bias as f32 * 0.01;
                self.tmp_min_lod = bti.header.min_lod as f32 * 0.125;
                self.tmp_max_lod = bti.header.max_lod as f32 * 0.125;
                self.sel_header = bti.header.clone();
                self.cur_header = bti.header.clone();

                self.png_as_image(bytes)?
            }
            _ => return Err(Error::InvalidFileType),
        };

        if let Some(handle) = self.image.as_ref() {
            ctx.tex_manager().write().free(handle.id());
        }
        let handle = ctx.load_texture(
            filename.to_string_lossy().to_string(),
            image_data,
            create_texture_options(
                self.sel_header.min_filter,
                self.sel_header.mag_filter,
                self.sel_header.wrap,
            ),
        );
        self.image = Some(handle);

        Ok(())
    }

    pub fn png_as_image<R: BufRead + Seek>(&self, bytes: R) -> Result<ImageData, Error> {
        let image = image::load(bytes, ImageFormat::Png)?;
        Ok(match image.color() {
            ColorType::L8 => ImageData::Color(Arc::new(ColorImage::from_gray(
                [
                    self.sel_header.width as usize,
                    self.sel_header.height as usize,
                ],
                image.as_bytes(),
            ))),
            ColorType::La8 => ImageData::Color(Arc::new(color_image_gray_alpha(
                [
                    self.sel_header.width as usize,
                    self.sel_header.height as usize,
                ],
                image.as_bytes(),
            ))),
            ColorType::Rgb8 => ImageData::Color(Arc::new(ColorImage::from_rgb(
                [
                    self.sel_header.width as usize,
                    self.sel_header.height as usize,
                ],
                image.as_bytes(),
            ))),
            ColorType::Rgba8 => ImageData::Color(Arc::new(ColorImage::from_rgba_unmultiplied(
                [
                    self.sel_header.width as usize,
                    self.sel_header.height as usize,
                ],
                image.as_bytes(),
            ))),
            ColorType::L16 => {
                let image = image.to_luma8();
                ImageData::Color(Arc::new(ColorImage::from_gray(
                    [
                        self.sel_header.width as usize,
                        self.sel_header.height as usize,
                    ],
                    image.as_raw(),
                )))
            }
            ColorType::La16 => {
                let image = image.to_luma_alpha8();
                ImageData::Color(Arc::new(color_image_gray_alpha(
                    [
                        self.sel_header.width as usize,
                        self.sel_header.height as usize,
                    ],
                    image.as_raw(),
                )))
            }
            ColorType::Rgb16 => {
                let image = image.to_rgb8();
                ImageData::Color(Arc::new(ColorImage::from_rgb(
                    [
                        self.sel_header.width as usize,
                        self.sel_header.height as usize,
                    ],
                    image.as_raw(),
                )))
            }
            ColorType::Rgba16 => {
                let image = image.to_rgba8();
                ImageData::Color(Arc::new(ColorImage::from_rgba_unmultiplied(
                    [
                        self.sel_header.width as usize,
                        self.sel_header.height as usize,
                    ],
                    image.as_raw(),
                )))
            }
            ColorType::Rgb32F => {
                let image = image.to_rgb8();
                ImageData::Color(Arc::new(ColorImage::from_rgb(
                    [
                        self.sel_header.width as usize,
                        self.sel_header.height as usize,
                    ],
                    image.as_raw(),
                )))
            }
            ColorType::Rgba32F => {
                let image = image.to_rgba8();
                ImageData::Color(Arc::new(ColorImage::from_rgba_unmultiplied(
                    [
                        self.sel_header.width as usize,
                        self.sel_header.height as usize,
                    ],
                    image.as_raw(),
                )))
            }
            _ => unreachable!("invalid image type found!"),
        })
    }
}

impl Default for MainApp {
    fn default() -> Self {
        Self {
            transform: TSTransform::default(),
            path: Default::default(),
            orig_png: None,
            image: None,
            cur_header: Header {
                ..Default::default()
            },
            sel_header: Header {
                ..Default::default()
            },
            tmp_lod_bias: 0.0,
            tmp_min_lod: 0.0,
            tmp_max_lod: 0.0,
            last_pos: None,
            first_run: true,
            show_about: false,
            err: String::new(),
        }
    }
}

impl eframe::App for MainApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        unsafe {
            #[allow(static_mut_refs)]
            if let Some(path) = OPEN_WITH.take() {
                if let Err(e) = self.process_new_file(ctx, path) {
                    self.err = format!("E10: {}", e);
                }
            }
        }
        if self.show_about {
            ctx.show_viewport_immediate(
                egui::ViewportId::from_hash_of("about"),
                egui::ViewportBuilder::default()
                    .with_title("About BTIZ")
                    .with_maximize_button(false)
                    .with_resizable(false)
                    .with_icon(egui::IconData {
                        rgba: ICON_PNG.to_vec(),
                        width: 128,
                        height: 128,
                    })
                    .with_inner_size([350.0, 243.0]),
                |ctx, _class| {
                    egui::CentralPanel::default().show(ctx, |ui| {
                        ui.vertical_centered(|ui| {
                            ui.label("Thanks to anyone who did any BTI research or documenting!");
                            ui.label("(I probably looked over your docs at one point)");
                            ui.label(" ");
                            ui.label("Special Thanks:");
                            ui.label("wiki.tockdom.com");
                            ui.label("noclip.website");
                            ui.label("LagoLunatic");
                            ui.label("SuperBMD");
                            ui.label("hitmen.c02.at");
                            ui.label("SuperHackio");
                            ui.label("libogc");
                            ui.label(" ");
                            ui.label(format!("BTIZ v{} (c) Zenny3D 2024", clap::crate_version!()));
                        });
                    });

                    if ctx.input(|i| i.viewport().close_requested()) {
                        // Tell parent viewport that we should not show next frame:
                        self.show_about = false;
                    }
                },
            );
        }
        if self.err.len() > 0 {
            ctx.show_viewport_immediate(
                egui::ViewportId::from_hash_of("err"),
                egui::ViewportBuilder::default()
                    .with_title("Error!")
                    .with_maximize_button(false)
                    .with_resizable(false)
                    .with_icon(egui::IconData {
                        rgba: ICON_PNG.to_vec(),
                        width: 128,
                        height: 128,
                    })
                    .with_inner_size([350.0, 120.0]),
                |ctx, _class| {
                    egui::CentralPanel::default().show(ctx, |ui| {
                        ui.centered_and_justified(|ui| {
                            ui.label(format!("Error: {}", self.err));
                        });
                    });

                    if ctx.input(|i| i.viewport().close_requested()) {
                        // Tell parent viewport that we should not show next frame:
                        self.err.clear();
                    }
                },
            );
        }
        {
            let layer_id = LayerId::new(Order::Background, "img_layer".into());
            let rect = Rect::from_two_pos(
                Pos2::new(0.0, 24.0),
                Pos2::new(0.0 + 836.0 + 16.0, 24.0 + 608.0 + 16.0),
            );

            let clip_rect = rect;
            let ui = Ui::new(ctx.clone(), layer_id, "tex_ui".into(), rect, clip_rect);
            ui.input(|input| {
                if input.pointer.latest_pos().is_some_and(|p| rect.contains(p)) {
                    if (input.raw_scroll_delta.y > 0.0 && self.transform.scaling <= 10.0)
                        || (input.raw_scroll_delta.y < 0.0 && self.transform.scaling > 0.1)
                    {
                        self.transform = self.transform
                            * TSTransform::from_scaling(1.0 + (input.raw_scroll_delta.y * 0.001));
                    }
                    let latest_pos = input.pointer.latest_pos().unwrap();
                    if input.pointer.press_origin().is_some() {
                        let move_vec = self.last_pos.unwrap_or(latest_pos.clone()) - latest_pos;
                        self.transform = self.transform
                            * TSTransform::from_translation(
                                move_vec * self.transform.inverse().scaling,
                            );
                    }
                    self.last_pos = Some(latest_pos);
                }
            });
            let img_center = rect.center()
                - self.transform.mul_pos(Pos2::new(
                    self.sel_header.width as f32 * 0.5,
                    self.sel_header.height as f32 * 0.5,
                ));
            let fixed_pos = self
                .transform
                .inverse()
                .mul_pos(Pos2::new(img_center.x, img_center.y));
            let new_rect = Rect::from_two_pos(
                Pos2::new(fixed_pos.x, fixed_pos.y),
                Pos2::new(fixed_pos.x + 836.0, fixed_pos.y + 608.0),
            );
            let mut ui = Ui::new(ctx.clone(), layer_id, "tex_pos".into(), new_rect, clip_rect);
            ui.set_clip_rect(self.transform.inverse() * rect);
            if let Some(img) = self.image.as_ref() {
                ui.image(ImageSource::Texture(SizedTexture::from(img)));
            }
            ui.ctx().set_transform_layer(layer_id, self.transform);
        }

        egui::TopBottomPanel::top("menu").resizable(false).show(ctx, |ui| {
            egui::menu::bar(ui, |ui| {
                let mut s = (*ctx.style()).clone();
                s.visuals.widgets.inactive.weak_bg_fill = Color32::from_gray(48);
                s.visuals.widgets.inactive.bg_fill = Color32::from_gray(48);
                s.visuals.widgets.active.bg_stroke.width = 0.0;
                s.visuals.widgets.hovered.bg_stroke.width = 0.0;
                s.visuals.widgets.hovered.rounding = Rounding::same(0.0);
                s.visuals.widgets.active.rounding = Rounding::same(0.0);
                s.visuals.widgets.inactive.rounding = Rounding::same(0.0);
                ctx.set_style(s);
                egui::menu::menu_button(ui, "File", |ui| {
                    if ui.button("Open...").clicked() {
                        if let Some(path) = rfd::FileDialog::new()
                            .add_filter("PNG or BTI", &["png", "bti"])
                            .pick_file() {
                            if let Err(e) = self.process_new_file(ctx, path) {
                                self.err = format!("E05: {}", e);
                            }
                        }
                        ui.close_menu();
                    }
                    if ui.button("Export PNG...").clicked() {
                        if let Some(png) = self.orig_png.as_ref() {
                            if let Some(path) = rfd::FileDialog::new()
                                .add_filter("PNG", &["png"])
                                .save_file() {
                                let bytes = BufReader::new(Cursor::new(png));

                                let bti = match Bti::read_png(
                                    path.clone(),
                                    bytes,
                                    BtiCompanion::new(None, self.cur_header.clone()),
                                ) {
                                    Ok(v) => v,
                                    Err(e) => {
                                        self.err = format!("E04: {}", e);
                                        return;
                                    }
                                };
                                if let Some(dir) = path.parent() {
                                    if let Err(e) = bti.write_png(&dir.to_path_buf()) {
                                        self.err = format!("E06: {}", e);
                                    }
                                } else {
                                    self.err = "E08: Selected file does not have a parent directory.".to_string();
                                }
                            }
                        } else {
                            self.err = "E07: Cannot save right now due to uninitialized data. Did you open a file?".to_string();
                        }
                        ui.close_menu();
                    }
                    if ui.button("Export BTI...").clicked() {
                        if let Some(png) = self.orig_png.as_ref() {
                            if let Some(path) = rfd::FileDialog::new()
                                .add_filter("BTI", &["bti"])
                                .save_file() {
                                let bytes = BufReader::new(Cursor::new(png));

                                let bti = match Bti::read_png(
                                    path.clone(),
                                    bytes,
                                    BtiCompanion::new(None, self.cur_header.clone()),
                                ) {
                                    Ok(v) => v,
                                    Err(e) => {
                                        self.err = format!("E04: {}", e);
                                        return;
                                    }
                                };

                                let mut data = vec![];
                                let mut cursor = Cursor::new(&mut data);
                                if let Err(e) = bti.write_bti(&mut cursor) {
                                    self.err = format!("E09: {}", e);
                                } else {
                                    if let Err(e) = fs::write(path, data) {
                                        self.err = format!("E11: Failed to write BTI: {}", e);
                                    }
                                }
                            }
                        } else {
                            self.err = "E10: Cannot save right now due to uninitialized data. Did you open a file?".to_string();
                        }
                        ui.close_menu();
                    }
                    if ui.button("Quit").clicked() {
                        std::process::exit(0);
                    }
                });
                egui::menu::menu_button(ui, "Help", |ui| {
                    if ui.button("About...").clicked() {
                        self.show_about = true;
                        ui.close_menu();
                    }
                });
            });
        });

        egui::SidePanel::right("props")
            .exact_width(300.0)
            .resizable(false)
            .show(ctx, |ui| {
                ui.heading("Properties");

                ui.horizontal(|ui| {
                    ui.with_layout(Layout::left_to_right(Align::BOTTOM), |ui| {
                        egui::Grid::new("format_grid")
                            .min_col_width(COL_WIDTH)
                            .num_columns(2)
                            .show(ui, |ui| {
                                ui.label("Format: ").on_hover_text(hover::FORMAT);
                                egui::ComboBox::from_id_source("sel_format")
                                    .selected_text(format!("{:?}", self.sel_header.format))
                                    .show_ui(ui, |ui| {
                                        ValidTextureFormat::select(ui, &mut self.sel_header.format);
                                    });
                            });
                    });
                });

                ui.horizontal(|ui| {
                    ui.with_layout(Layout::left_to_right(Align::BOTTOM), |ui| {
                        egui::Grid::new("alpha_grid")
                            .min_col_width(COL_WIDTH)
                            .num_columns(2)
                            .show(ui, |ui| {
                                ui.label("Alpha: ").on_hover_text(hover::ALPHA);
                                egui::ComboBox::from_id_source("sel_alpha")
                                    .selected_text(format!("{:?}", self.sel_header.alpha))
                                    .show_ui(ui, |ui| {
                                        ui.selectable_value(
                                            &mut self.sel_header.alpha,
                                            JUTTransparency::Opaque,
                                            "Opaque",
                                        );
                                        ui.selectable_value(
                                            &mut self.sel_header.alpha,
                                            JUTTransparency::Translucent,
                                            "Translucent",
                                        );
                                        ui.selectable_value(
                                            &mut self.sel_header.alpha,
                                            JUTTransparency::Clip,
                                            "Clip",
                                        );
                                        ui.selectable_value(
                                            &mut self.sel_header.alpha,
                                            JUTTransparency::Bmd,
                                            "Bmd",
                                        );
                                    });
                            });
                    });
                });

                ui.horizontal(|ui| {
                    ui.with_layout(Layout::left_to_right(Align::BOTTOM), |ui| {
                        egui::Grid::new("wraps_grid")
                            .min_col_width(COL_WIDTH)
                            .num_columns(2)
                            .show(ui, |ui| {
                                ui.label("Wrap S: ").on_hover_text(hover::WRAP_S);
                                egui::ComboBox::from_id_source("sel_wraps")
                                    .selected_text(format!("{:?}", self.sel_header.wrap.0))
                                    .show_ui(ui, |ui| {
                                        ui.selectable_value(
                                            &mut self.sel_header.wrap.0,
                                            TextureWrap::Repeat,
                                            "Repeat",
                                        );
                                        ui.selectable_value(
                                            &mut self.sel_header.wrap.0,
                                            TextureWrap::Mirror,
                                            "Mirror",
                                        );
                                        ui.selectable_value(
                                            &mut self.sel_header.wrap.0,
                                            TextureWrap::Clamp,
                                            "Clamp",
                                        );
                                    });
                            });
                    });
                });

                ui.horizontal(|ui| {
                    ui.with_layout(Layout::left_to_right(Align::BOTTOM), |ui| {
                        egui::Grid::new("wrapt_grid")
                            .min_col_width(COL_WIDTH)
                            .num_columns(2)
                            .show(ui, |ui| {
                                ui.label("Wrap T: ").on_hover_text(hover::WRAP_T);
                                egui::ComboBox::from_id_source("sel_wrapt")
                                    .selected_text(format!("{:?}", self.sel_header.wrap.1))
                                    .show_ui(ui, |ui| {
                                        ui.selectable_value(
                                            &mut self.sel_header.wrap.1,
                                            TextureWrap::Repeat,
                                            "Repeat",
                                        );
                                        ui.selectable_value(
                                            &mut self.sel_header.wrap.1,
                                            TextureWrap::Mirror,
                                            "Mirror",
                                        );
                                        ui.selectable_value(
                                            &mut self.sel_header.wrap.1,
                                            TextureWrap::Clamp,
                                            "Clamp",
                                        );
                                    });
                            });
                    });
                });

                if self.sel_header.format.use_palette() {
                    ui.horizontal(|ui| {
                        ui.with_layout(Layout::left_to_right(Align::BOTTOM), |ui| {
                            egui::Grid::new("pformat_grid")
                                .min_col_width(COL_WIDTH)
                                .num_columns(2)
                                .show(ui, |ui| {
                                    ui.label("Palette Format: ").on_hover_text(hover::PALETTE_FORMAT);
                                    egui::ComboBox::from_id_source("sel_pformat")
                                        .selected_text(format!(
                                            "{:?}",
                                            self.sel_header.palette_format
                                        ))
                                        .show_ui(ui, |ui| {
                                            ui.selectable_value(
                                                &mut self.sel_header.palette_format,
                                                PaletteFormat::IA8,
                                                "IA8",
                                            );
                                            ui.selectable_value(
                                                &mut self.sel_header.palette_format,
                                                PaletteFormat::RGB5A3,
                                                "RGB5A3",
                                            );
                                            ui.selectable_value(
                                                &mut self.sel_header.palette_format,
                                                PaletteFormat::RGB565,
                                                "RGB565",
                                            );
                                        });
                                });
                        });
                    });
                }

                ui.horizontal(|ui| {
                    ui.with_layout(Layout::left_to_right(Align::BOTTOM), |ui| {
                        egui::Grid::new("mip_grid")
                            .min_col_width(COL_WIDTH)
                            .num_columns(2)
                            .show(ui, |ui| {
                                ui.label("Mipmap: ").on_hover_text(hover::MIPMAP);
                                egui::Checkbox::new(&mut self.sel_header.mipmap, "").ui(ui);
                            });
                    });
                });

                if self.sel_header.mipmap {
                    ui.horizontal(|ui| {
                        ui.with_layout(Layout::left_to_right(Align::BOTTOM), |ui| {
                            egui::Grid::new("nmip_grid")
                                .min_col_width(COL_WIDTH)
                                .num_columns(2)
                                .show(ui, |ui| {
                                    ui.label("# Mipmaps: ").on_hover_text(hover::N_MIPMAPS);
                                    egui::Slider::new(&mut self.sel_header.mipmap_count, 1..=8)
                                        .ui(ui);
                                });
                        });
                    });
                }

                ui.horizontal(|ui| {
                    ui.with_layout(Layout::left_to_right(Align::BOTTOM), |ui| {
                        egui::Grid::new("elod_grid")
                            .min_col_width(COL_WIDTH)
                            .num_columns(2)
                            .show(ui, |ui| {
                                ui.label("Edge LOD: ").on_hover_text(hover::EDGE_LOD);
                                egui::Checkbox::new(&mut self.sel_header.edge_lod, "").ui(ui);
                            });
                    });
                });

                ui.horizontal(|ui| {
                    ui.with_layout(Layout::left_to_right(Align::BOTTOM), |ui| {
                        egui::Grid::new("blod_grid")
                            .min_col_width(COL_WIDTH)
                            .num_columns(2)
                            .show(ui, |ui| {
                                ui.label("LOD Bias: ").on_hover_text(hover::LOD_BIAS);
                                egui::Slider::new(
                                    &mut self.tmp_lod_bias,
                                    (i16::MIN as f32 * 0.01)..=(i16::MAX as f32 * 0.01),
                                )
                                .step_by(0.01)
                                .min_decimals(2)
                                .max_decimals(2)
                                .ui(ui);
                            });
                    });
                });

                ui.horizontal(|ui| {
                    ui.with_layout(Layout::left_to_right(Align::BOTTOM), |ui| {
                        egui::Grid::new("bclamp_grid")
                            .min_col_width(COL_WIDTH)
                            .num_columns(2)
                            .show(ui, |ui| {
                                ui.label("LOD Bias Clamp: ").on_hover_text(hover::LOD_BIAS_CLAMP);
                                egui::Checkbox::new(&mut self.sel_header.bias_clamp, "").ui(ui);
                            });
                    });
                });

                ui.horizontal(|ui| {
                    ui.with_layout(Layout::left_to_right(Align::BOTTOM), |ui| {
                        egui::Grid::new("nlod_grid")
                            .min_col_width(COL_WIDTH)
                            .num_columns(2)
                            .show(ui, |ui| {
                                ui.label("Min LOD: ").on_hover_text(hover::MIN_LOD);
                                egui::Slider::new(&mut self.tmp_min_lod, 0.0..=10.0)
                                    .min_decimals(3)
                                    .max_decimals(3)
                                    .step_by(0.125)
                                    .ui(ui);
                            });
                    });
                });

                ui.horizontal(|ui| {
                    ui.with_layout(Layout::left_to_right(Align::BOTTOM), |ui| {
                        egui::Grid::new("xlod_grid")
                            .min_col_width(COL_WIDTH)
                            .num_columns(2)
                            .show(ui, |ui| {
                                ui.label("Max LOD: ").on_hover_text(hover::MAX_LOD);
                                egui::Slider::new(&mut self.tmp_max_lod, 0.0..=10.0)
                                    .min_decimals(3)
                                    .max_decimals(3)
                                    .step_by(0.125)
                                    .ui(ui);
                            });
                    });
                });

                ui.horizontal(|ui| {
                    ui.with_layout(Layout::left_to_right(Align::BOTTOM), |ui| {
                        egui::Grid::new("ani_grid")
                            .min_col_width(COL_WIDTH)
                            .num_columns(2)
                            .show(ui, |ui| {
                                ui.label("Anisotropy: ").on_hover_text(hover::ANISOTROPY);
                                egui::ComboBox::from_id_source("sel_ani")
                                    .selected_text(format!("{:?}", self.sel_header.max_anisotropy))
                                    .show_ui(ui, |ui| {
                                        ui.selectable_value(
                                            &mut self.sel_header.max_anisotropy,
                                            Anisotropy::X1,
                                            "X1",
                                        );
                                        ui.selectable_value(
                                            &mut self.sel_header.max_anisotropy,
                                            Anisotropy::X2,
                                            "X2",
                                        );
                                        ui.selectable_value(
                                            &mut self.sel_header.max_anisotropy,
                                            Anisotropy::X4,
                                            "X4",
                                        )
                                    });
                            });
                    });
                });

                ui.horizontal(|ui| {
                    ui.with_layout(Layout::left_to_right(Align::BOTTOM), |ui| {
                        egui::Grid::new("nfilter_grid")
                            .min_col_width(COL_WIDTH)
                            .num_columns(2)
                            .show(ui, |ui| {
                                ui.label("Min Filter: ").on_hover_text(hover::MIN_FILTER);
                                egui::ComboBox::from_id_source("sel_nfilter")
                                    .selected_text(format!("{:?}", self.sel_header.min_filter))
                                    .show_ui(ui, |ui| {
                                        ui.selectable_value(
                                            &mut self.sel_header.min_filter,
                                            TextureFilter::Linear,
                                            "Linear",
                                        );
                                        ui.selectable_value(
                                            &mut self.sel_header.min_filter,
                                            TextureFilter::Near,
                                            "Near",
                                        );
                                        ui.selectable_value(
                                            &mut self.sel_header.min_filter,
                                            TextureFilter::NearMipNear,
                                            "NearMipNear",
                                        );
                                        ui.selectable_value(
                                            &mut self.sel_header.min_filter,
                                            TextureFilter::LinearMipNear,
                                            "LinearMipNear",
                                        );
                                        ui.selectable_value(
                                            &mut self.sel_header.min_filter,
                                            TextureFilter::NearMipLinear,
                                            "NearMipLinear",
                                        );
                                        ui.selectable_value(
                                            &mut self.sel_header.min_filter,
                                            TextureFilter::LinearMipLinear,
                                            "LinearMipLinear",
                                        );
                                    });
                            });
                    });
                });

                ui.horizontal(|ui| {
                    ui.with_layout(Layout::left_to_right(Align::BOTTOM), |ui| {
                        egui::Grid::new("gfilter_grid")
                            .min_col_width(COL_WIDTH)
                            .num_columns(2)
                            .show(ui, |ui| {
                                ui.label("Mag Filter: ").on_hover_text(hover::MAG_FILTER);
                                egui::ComboBox::from_id_source("sel_gfilter")
                                    .selected_text(format!("{:?}", self.sel_header.mag_filter))
                                    .show_ui(ui, |ui| {
                                        ValidMagFilter::select(ui, &mut self.sel_header.mag_filter)
                                    });
                            });
                    });
                });

                ui.horizontal(|ui| {
                    ui.with_layout(Layout::left_to_right(Align::BOTTOM), |ui| {
                        egui::Grid::new("unk_grid")
                            .min_col_width(COL_WIDTH)
                            .num_columns(2)
                            .show(ui, |ui| {
                                ui.label("Unknown: ").on_hover_text(hover::UNKNOWN);
                                egui::Slider::new(&mut self.sel_header.unknown, 0..=u8::MAX).ui(ui);
                            });
                    });
                });
            });

        if let Some(png) = self.orig_png.as_ref() {
            let new_bias = ((self.tmp_lod_bias * 100.0) + f32::EPSILON) as i16;
            let new_min = ((self.tmp_min_lod * 8.0) + f32::EPSILON) as i8;
            let new_max = ((self.tmp_max_lod * 8.0) + f32::EPSILON) as i8;

            let changed_at_all = self.first_run
                || self.cur_header != self.sel_header
                || self.cur_header.lod_bias != new_bias
                || self.cur_header.min_lod != new_min
                || self.cur_header.max_lod != new_max;

            let mut changed_visual = false;
            if self.first_run
                || self.cur_header.format != self.sel_header.format
                || self.cur_header.palette != self.sel_header.palette
                || self.cur_header.palette_format != self.sel_header.palette_format
                || self.cur_header.alpha != self.sel_header.alpha
                || self.cur_header.min_filter != self.sel_header.min_filter
                || self.cur_header.mag_filter != self.sel_header.mag_filter
            {
                // something visible changed
                changed_visual = true;
                self.first_run = false;
            }

            if changed_at_all {
                self.cur_header = self.sel_header.clone();
                // Add epsilon just in case it's slightly off because floats
                self.cur_header.lod_bias = new_bias;
                self.cur_header.min_lod = new_min;
                self.cur_header.max_lod = new_max;

                // update the image
                if changed_visual {
                    let filename = self
                        .path
                        .file_name()
                        .map(|v| v.to_string_lossy().to_string())
                        .unwrap_or("tmp_cur".to_string());

                    let bytes = BufReader::new(Cursor::new(png));

                    let bti = match Bti::read_png(
                        self.path.clone(),
                        bytes,
                        BtiCompanion::new(None, self.cur_header.clone()),
                    ) {
                        Ok(v) => v,
                        Err(e) => {
                            self.err = format!("E04: {}", e);
                            return;
                        }
                    };

                    let (image_type, img_data) = match bti.generate_png_data() {
                        Ok(v) => v,
                        Err(e) => {
                            self.err = format!("E03: {}", e);
                            return;
                        }
                    };

                    let mut png_bytes = Vec::new();
                    let mut png_buf = Cursor::new(&mut png_bytes);

                    match image::write_buffer_with_format(
                        &mut png_buf,
                        &img_data,
                        self.cur_header.width as u32,
                        self.cur_header.height as u32,
                        image_type,
                        ImageFormat::Png,
                    ) {
                        Ok(v) => v,
                        Err(e) => {
                            self.err = format!("E02: {}", e);
                            return;
                        }
                    };

                    let png_buf = BufReader::new(Cursor::new(&png_bytes));

                    let image_data = match self.png_as_image(png_buf) {
                        Ok(v) => v,
                        Err(e) => {
                            self.err = format!("E01: {}", e);
                            return;
                        }
                    };

                    if let Some(handle) = self.image.as_ref() {
                        ctx.tex_manager().write().free(handle.id());
                    }
                    let handle = ctx.load_texture(
                        filename,
                        image_data,
                        create_texture_options(
                            self.sel_header.min_filter,
                            self.sel_header.mag_filter,
                            self.sel_header.wrap,
                        ),
                    );
                    self.image = Some(handle);
                }
            }
        }
    }
}
