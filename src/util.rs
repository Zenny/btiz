use std::io::{BufReader, Cursor};
use std::path::PathBuf;
use std::str::FromStr;

use eframe::egui::{Color32, ColorImage, TextBuffer, TextureOptions, Ui};
use epaint::textures::TextureWrapMode;
use gczen::companion::bti::BtiCompanion;
use gczen::gc::bti::{Bti, Header};
use gczen::gc::types::bti::{JUTTransparency, TextureFilter, TextureFormat, TextureWrap};
use clap::ValueEnum;
use image::{ColorType, ImageFormat};

use crate::error::Error;

pub fn color_image_gray_alpha(size: [usize; 2], gray: &[u8]) -> ColorImage {
    assert_eq!(size[0] * size[1] * 2, gray.len());
    let pixels = gray
        .chunks_exact(2)
        .map(|p| Color32::from_rgba_unmultiplied(p[0], p[0], p[0], p[1]))
        .collect();

    ColorImage { size, pixels }
}

pub fn create_texture_options(
    min: TextureFilter,
    mag: TextureFilter,
    wrap: (TextureWrap, TextureWrap),
) -> TextureOptions {
    TextureOptions {
        magnification: gcn_filter_to_epaint_approx(mag),
        minification: gcn_filter_to_epaint_approx(min),
        wrap_mode: gcn_wrap_to_epaint_approx(wrap),
    }
}

pub fn gcn_filter_to_epaint_approx(f: TextureFilter) -> epaint::textures::TextureFilter {
    match f {
        TextureFilter::Near => epaint::textures::TextureFilter::Nearest,
        TextureFilter::Linear => epaint::textures::TextureFilter::Linear,
        TextureFilter::NearMipNear => epaint::textures::TextureFilter::Nearest,
        TextureFilter::LinearMipNear => epaint::textures::TextureFilter::Linear,
        TextureFilter::NearMipLinear => epaint::textures::TextureFilter::Nearest,
        TextureFilter::LinearMipLinear => epaint::textures::TextureFilter::Linear,
    }
}

pub fn gcn_wrap_to_epaint_approx(w: (TextureWrap, TextureWrap)) -> TextureWrapMode {
    if w.0 == TextureWrap::Mirror || w.1 == TextureWrap::Mirror {
        TextureWrapMode::MirroredRepeat
    } else if w.0 == TextureWrap::Repeat || w.1 == TextureWrap::Repeat {
        TextureWrapMode::Repeat
    } else {
        TextureWrapMode::ClampToEdge
    }
}

/// Valid texture formats for this editor
#[derive(Debug, Copy, Clone, PartialEq, Eq, ValueEnum)]
pub enum ValidTextureFormat {
    /// 1 channel (gray), 4 bits per channel
    I4,
    /// 1 channel (gray), 8 bits per channel
    I8,
    /// 2 channels (gray, alpha), 4 bits per channel
    IA4,
    /// 2 channels (gray, alpha), 8 bits per channel
    IA8,
    /// 3 channels (rgb), 5 bit red/blue, 6 bit green
    RGB565,
    /// 4 channels (rgba), 5 bit color, 3 bit alpha
    RGB5A3,
    /// 4 channels (rgba), 8 bits per channel
    RGBA8,
    /// Palette-indexed colors, 8 bit index
    C8,
    /// Block-compressed colors
    Compressed,
}

impl ValidTextureFormat {
    pub fn select(ui: &mut Ui, format: &mut TextureFormat) {
        ui.selectable_value(
            format,
            TextureFormat::I4,
            "I4",
        );
        ui.selectable_value(
            format,
            TextureFormat::I8,
            "I8",
        );
        ui.selectable_value(
            format,
            TextureFormat::IA4,
            "IA4",
        );
        ui.selectable_value(
            format,
            TextureFormat::IA8,
            "IA8",
        );
        ui.selectable_value(
            format,
            TextureFormat::RGB565,
            "RGB565",
        );
        ui.selectable_value(
            format,
            TextureFormat::RGB5A3,
            "RGB5A3",
        );
        ui.selectable_value(
            format,
            TextureFormat::RGBA8,
            "RGBA8",
        );
        // todo c4 support (sms never uses it) (it might be easy tho)
        ui.selectable_value(
            format,
            TextureFormat::C8,
            "C8",
        );
        ui.selectable_value(
            format,
            TextureFormat::Compressed,
            "Compressed",
        );
    }
}

impl TryFrom<TextureFormat> for ValidTextureFormat {
    type Error = String;

    fn try_from(value: TextureFormat) -> Result<Self, Self::Error> {
        Ok(match value {
            TextureFormat::I4 => Self::I4,
            TextureFormat::I8 => Self::I8,
            TextureFormat::C8 => Self::C8,
            TextureFormat::RGBA8 => Self::RGBA8,
            TextureFormat::RGB565 => Self::RGB565,
            TextureFormat::RGB5A3 => Self::RGB5A3,
            TextureFormat::Compressed => Self::Compressed,
            _ => return Err(unsafe { String::from_str("Invalid texture format conversion").unwrap_unchecked() }),
        })
    }
}

impl Into<TextureFormat> for ValidTextureFormat {
    fn into(self) -> TextureFormat {
        match self {
            Self::I4 => TextureFormat::I4,
            Self::I8 => TextureFormat::I8,
            Self::IA4 => TextureFormat::IA4,
            Self::IA8 => TextureFormat::IA8,
            Self::RGB565 => TextureFormat::RGB565,
            Self::RGB5A3 => TextureFormat::RGB5A3,
            Self::RGBA8 => TextureFormat::RGBA8,
            Self::C8 => TextureFormat::C8,
            Self::Compressed => TextureFormat::Compressed,
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, ValueEnum)]
pub enum ValidMagFilter {
    /// Point sampling, no mipmap
    Near,
    /// Bilinear filtering, no mipmap
    Linear,
}

impl ValidMagFilter {
    pub fn select(ui: &mut Ui, filter: &mut TextureFilter) {
        ui.selectable_value(
            filter,
            TextureFilter::Linear,
            "Linear",
        );
        ui.selectable_value(
            filter,
            TextureFilter::Near,
            "Near",
        );
    }
}

impl TryFrom<TextureFilter> for ValidMagFilter {
    type Error = String;

    fn try_from(value: TextureFilter) -> Result<Self, Self::Error> {
        Ok(match value {
            TextureFilter::Near => Self::Near,
            TextureFilter::Linear => Self::Linear,
            _ => return Err(unsafe { String::from_str("Invalid texture format conversion").unwrap_unchecked() }),
        })
    }
}

impl Into<TextureFilter> for ValidMagFilter {
    fn into(self) -> TextureFilter {
        match self {
            Self::Linear => TextureFilter::Linear,
            Self::Near => TextureFilter::Near,
        }
    }
}

pub fn read_as_bti(path: PathBuf, data: &[u8]) -> Result<Bti, Error> {
    let Some(ext) = path.extension() else {
        return Err(Error::InvalidFileType);
    };

    match ext.to_string_lossy().as_str() {
        "png" => {
            let companion = match BtiCompanion::read(&path) {
                Ok(v) => v,
                Err(_) => {
                    let image = image::load_from_memory_with_format(data, ImageFormat::Png)?;
                    let (format, alpha) = match image.color() {
                        ColorType::L8 => (TextureFormat::I8, JUTTransparency::Opaque),
                        ColorType::La8 => (TextureFormat::IA8, JUTTransparency::Translucent),
                        ColorType::Rgb8 => (TextureFormat::RGB565, JUTTransparency::Opaque),
                        ColorType::Rgba8 => {
                            (TextureFormat::Compressed, JUTTransparency::Translucent)
                        }
                        ColorType::L16 => (TextureFormat::I8, JUTTransparency::Opaque),
                        ColorType::La16 => (TextureFormat::IA8, JUTTransparency::Translucent),
                        ColorType::Rgb16 => {
                            (TextureFormat::Compressed, JUTTransparency::Opaque)
                        }
                        ColorType::Rgba16 => {
                            (TextureFormat::Compressed, JUTTransparency::Translucent)
                        }
                        ColorType::Rgb32F => {
                            (TextureFormat::Compressed, JUTTransparency::Opaque)
                        }
                        ColorType::Rgba32F => {
                            (TextureFormat::Compressed, JUTTransparency::Translucent)
                        }
                        _ => unreachable!("meta else unknown image type"),
                    };

                    BtiCompanion::new(
                        None,
                        Header {
                            format,
                            alpha,
                            width: image.width() as u16,
                            height: image.height() as u16,
                            wrap: (Default::default(), Default::default()),
                            palette: false,
                            palette_format: Default::default(),
                            palette_count: 0,
                            palette_offset: 0,
                            mipmap: false,
                            edge_lod: false,
                            bias_clamp: false,
                            max_anisotropy: Default::default(),
                            min_filter: Default::default(),
                            mag_filter: Default::default(),
                            min_lod: 0,
                            max_lod: 0,
                            mipmap_count: 1,
                            unknown: 0,
                            lod_bias: 0,
                            tex_data_offset: 0,
                        },
                    )
                }
            };
            Ok(Bti::read_png(path, BufReader::new(Cursor::new(data)), companion)?)
        }
        "bti" => {
            let mut data = Cursor::new(data);
            Ok(Bti::new(path, &mut data, 0, None)?)
        }
        _ => Err(Error::InvalidFileType),
    }
}

