use displaydoc::Display;
use gczen::{
    companion::error::CompanionError,
    gc::{
        bti::error::{BtiWriteError, PngReadError, PngWriteError},
        error::ReadError,
    },
};
use image::ImageError;
use std::io;
use thiserror::Error;

#[derive(Display, Error, Debug)]
pub enum Error {
    /// Failed to read: {0}
    Io(#[from] io::Error),
    /// Failed to write bti data: {0}
    BtiWrite(#[from] BtiWriteError),
    /// Failed to read bti file: {0}
    Bti(#[from] ReadError),
    /// Failed to write png data: {0}
    PngWrite(#[from] PngWriteError),
    /// Failed to read png data: {0}
    PngRead(#[from] PngReadError),
    /// Failed to read meta file: {0}
    Companion(#[from] CompanionError),
    /// Failed to load png file: {0}
    Image(#[from] ImageError),
    /// Invalid file type
    InvalidFileType,
    /// Specified file does not exist
    FileNotExist,
    /// Specified file already exists
    FileExist,
}
