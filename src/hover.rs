pub const FORMAT: &str = "The texture format.";
pub const ALPHA: &str = "The texture's alpha format.";
pub const WRAP_S: &str = "Horizontal axis wrap.";
pub const WRAP_T: &str = "Vertical axis wrap.";
pub const PALETTE_FORMAT: &str = "The color palette format.";
pub const MIPMAP: &str = "Whether smaller versions of the texture will be generated (for LOD).";
pub const N_MIPMAPS: &str = "How many mipmaps to generate.";
pub const EDGE_LOD: &str = "Compute LOD using adjacent texels. \
                                This should be set if bias clamp is set or if anisotropy is set to X2 or X4.";
pub const LOD_BIAS: &str = "The LOD bias of the texture.";
pub const LOD_BIAS_CLAMP: &str = "Clamp \
                                (LOD + lodbias) so that it is never less than the minimum extent of \
                                the pixel projected in texture space. Setting bias clamp prevents \
                                over-biasing the LOD when the polygon is perpendicular to the view \
                                direction.";
pub const MIN_LOD: &str = "The minimum LOD clamp, usually 0.";
pub const MAX_LOD: &str = "The maximum LOD clamp, usually (mipmap_count - 1).";
pub const ANISOTROPY: &str = "Anisotropic filtering, used to \
                                enhance the image quality of textures viewed at oblique angles.";
pub const MIN_FILTER: &str = "Texture filter type to use when the texel/pixel ratio is >= 1.0.";
pub const MAG_FILTER: &str = "Texture filter type to use when the texel/pixel ratio is < 1.0.";
pub const UNKNOWN: &str = "An unknown value.";

