use std::{fs, io};
use std::io::{BufReader, Cursor};
use std::path::PathBuf;
use std::process::exit;

use clap::*;
use const_format::concatcp;
use displaydoc::Display;
use gczen::companion::bti::BtiCompanion;
use gczen::gc::bti::{Bti, Header};
use gczen::gc::types::bti::{Anisotropy, JUTTransparency, PaletteFormat, TextureFilter, TextureFormat, TextureWrap};
use image::{save_buffer_with_format, write_buffer_with_format, ImageFormat};

use crate::error::Error;
use crate::util::{read_as_bti, ValidMagFilter, ValidTextureFormat};
use crate::hover;

macro_rules! range_help {
    ($num:ty) => {
        range_help!({<$num>::MIN}, {<$num>::MAX})
    };

    ($a:expr, $b:expr) => {
        concatcp!("[possible values: ", $a, " to ", $b, "]")
    }
}

macro_rules! range_help_lit {
    ($a:literal, $b:literal) => {
        concat!("[possible values: ", $a, " to ", $b, "]")
    };
    ($a:literal, $b:literal, $p:literal) => {
        concat!("[possible values: ", $a, " to ", $b, ", precision: ", $p,"]")
    };
}

fn num_mipmaps(s: &str) -> Result<u8, String> {
    let n: u8 = s.parse().map_err(|_| "num-mipmaps must be in range 0 to 7")?;
    if n > 7 {
        return Err("num-mipmaps must be in range 0 to 7".to_string());
    }
    Ok(n)
}

fn f32_lod_bias_i16(s: &str) -> Result<i16, String> {
    let f: f32 = s.parse().map_err(|_| "failed to parse lod-bias as float")?;
    if f < -327.68 || f > 327.67 {
        return Err("min-lod must be within range of -327.68 to 327.67".to_string());
    }

    let fin = (f * 100.0) + f32::EPSILON;
    let fin_floor = fin.floor();
    if (fin - fin_floor).abs() >= 0.01 {
        log::warn!("lod-bias will be snapped to nearest meaningful value: {}", fin_floor * 0.01);
    }

    Ok(fin_floor as i16)
}

fn f32_min_lod_i8(s: &str) -> Result<i8, String> {
    let f: f32 = s.parse().map_err(|_| "failed to parse min-lod as float")?;
    if f < 0.0 || f > 10.0 {
        return Err("min-lod must be within range of 0.0 to 10.0".to_string());
    }

    let fin = (f * 8.0) + f32::EPSILON;
    let fin_int = fin as i8;
    let fin_undo = fin_int as f32 * 0.125;
    if (fin - fin_undo) >= 0.00001 {
        log::warn!("min-lod will be snapped to nearest meaningful value: {}", fin_undo);
    }

    Ok(fin_int)
}

fn f32_max_lod_i8(s: &str) -> Result<i8, String> {
    let f: f32 = s.parse().map_err(|_| "failed to parse max-lod as float")?;
    if f < 0.0 || f > 10.0 {
        return Err("max-lod must be within range of 0.0 to 10.0".to_string());
    }

    let fin = (f * 8.0) + f32::EPSILON;
    let fin_int = fin as i8;
    let fin_undo = fin_int as f32 * 0.125;
    if (fin - fin_undo) >= 0.00001 {
        log::warn!("max-lod will be snapped to nearest meaningful value: {}", fin_undo);
    }

    Ok(fin_int)
}

#[derive(Clone, Parser, Debug)]
#[command(version, about, long_about = None)]
pub struct Args {
    #[arg(short, long)]
    input: PathBuf,

    #[arg(short, long)]
    output: Option<PathBuf>,

    #[arg(short, long, long_help = hover::FORMAT)]
    format: Option<ValidTextureFormat>,
    #[arg(short, long, long_help = hover::PALETTE_FORMAT)]
    palette_format: Option<PaletteFormat>,

    #[arg(short, long, long_help = hover::ALPHA)]
    alpha: Option<JUTTransparency>,

    #[arg(short = 's', long, long_help = hover::WRAP_S)]
    wrap_s: Option<TextureWrap>,
    #[arg(short = 't', long, long_help = hover::WRAP_T)]
    wrap_t: Option<TextureWrap>,
    /// 0 means mipmap = false
    #[arg(short, long, value_parser = num_mipmaps, help = concatcp!(hover::N_MIPMAPS, " ", range_help_lit!(0, 8)), long_help = concatcp!(hover::N_MIPMAPS, "\n\n", range_help_lit!(0, 8)))]
    mipmaps: Option<u8>,

    #[arg(short, long, long_help = hover::EDGE_LOD)]
    edge_lod: Option<Option<bool>>,

    #[arg(short = 'b', long, value_parser = f32_lod_bias_i16, help = concatcp!(hover::LOD_BIAS, " ", range_help_lit!(-327.68, 327.67, 0.01)), long_help = concatcp!(hover::LOD_BIAS, "\n\n", range_help_lit!(-327.68, 327.67, 0.01)))]
    lod_bias: Option<i16>,

    #[arg(short = 'c', long, long_help = hover::LOD_BIAS_CLAMP)]
    lod_bias_clamp: Option<Option<bool>>,
    
    #[arg(short = 'l', long, value_parser = f32_min_lod_i8, help = concatcp!(hover::MIN_LOD, " ", range_help_lit!(0.0, 10.0, 0.125)), long_help = concatcp!(hover::MIN_LOD, "\n\n", range_help_lit!(0.0, 10.0, 0.125)))]
    min_lod: Option<i8>,
    #[arg(short = 'd', long, value_parser = f32_max_lod_i8, help = concatcp!(hover::MAX_LOD, " ", range_help_lit!(0.0, 10.0, 0.125)), long_help = concatcp!(hover::MAX_LOD, "\n\n", range_help_lit!(0.0, 10.0, 0.125)))]
    max_lod: Option<i8>,

    #[arg(short = 'A', long, long_help = hover::ANISOTROPY)]
    anisotropy: Option<Anisotropy>,

    #[arg(short = 'n', long, long_help = hover::MIN_FILTER)]
    min_filter: Option<TextureFilter>,
    #[arg(short = 'g', long, long_help = hover::MAG_FILTER)]
    mag_filter: Option<ValidMagFilter>,

    #[arg(short = 'u', long, help = concatcp!(hover::UNKNOWN, " ", range_help!(u8)), long_help = concatcp!(hover::UNKNOWN, "\n\n", range_help!(u8)))]
    unknown: Option<u8>,

    #[arg(short = 'y', long, long_help = "Overwrite files if they already exist")]
    overwrite: Option<Option<bool>>,
}

#[derive(Display)]
enum Conversion {
    /// Converting from PNG to BTI
    PngToBti,
    /// Converting from BTI to PNG
    BtiToPng,
    /// Converting from BTI to BTI
    BtiEdit,
    /// Converting from PNG to PNG
    PngEdit,
}

#[inline]
fn bool_arg(b: Option<Option<bool>>) -> bool {
    b.is_some_and(|v| v.is_none_or(|i| i))
}

// todo this could probably be  more efficient for png
pub fn process_conversion(mut args: Args) -> Result<(), Error> {
    if !args.input.exists() {
        return Err(Error::FileNotExist);
    }

    // if ext is png and output not set, we do png->bti
    let conversion = if args.input.extension().is_some_and(|ex| ex == "png") && args.output.as_ref().is_none_or(|v| v.extension().is_some_and(|ex| ex == "bti")) {
        if args.output.is_none() {
            args.output = Some(args.input.clone().with_extension("bti"));
        }
        Conversion::PngToBti
    // bti->png
    } else if args.input.extension().is_some_and(|ex| ex == "bti") && args.output.as_ref().is_none_or(|v| v.extension().is_some_and(|ex| ex == "png")) {
        if args.output.is_none() {
            args.output = Some(args.input.clone().with_extension("png"));
        }
        Conversion::BtiToPng
    // png edit
    } else if args.input.extension().is_some_and(|ex| ex == "png") && args.output.as_ref().is_some_and(|v| v.extension().is_some_and(|ex| ex == "png")) {
        Conversion::PngEdit
    // bti edit
    } else if args.input.extension().is_some_and(|ex| ex == "bti") && args.output.as_ref().is_some_and(|v| v.extension().is_some_and(|ex| ex == "bti")) {
        Conversion::BtiEdit
    } else {
        log::error!("Could not determine a valid conversion method");
        exit(1);
    };

    let output = args.output.take().unwrap();
    if output.exists() && !bool_arg(args.overwrite) {
        let stdin = io::stdin();
        let mut yn = String::new();
        loop {
            log::info!("Overwrite {:?}? (y/n)", output);
            let _ = stdin.read_line(&mut yn)?;
            yn = yn.trim().to_lowercase();
            match yn.as_str() {
                "y" => {
                    break;
                }
                "n" => {
                    log::warn!("File exists. Aborting");
                    return Err(Error::FileExist);
                }
                _ => {
                    log::error!("Unknown response, please try again");
                    yn.clear();
                }
            }
        }
    }

    let data = fs::read(&args.input)?;
    // This also gives us defaults for pngs without companion files
    let bti = read_as_bti(args.input.clone(), &data)?;
    // we don't want to change the OG bti header because we need it for png_data
    let mut header = bti.header.clone();
    set_header(&mut header, args.clone());

    log::info!("{}", conversion);
    log::info!("Using header info:\n{:#?}", header);
    
    // todo can maybe be split into InputType::Bti OutputType::Bti etc, because the code is shared
    match conversion {
        Conversion::PngToBti => {
            let data = BufReader::new(Cursor::new(&data));
            log::info!("Encoding data to proper format...");
            let conv = Bti::read_png(args.input.clone(), data, BtiCompanion::new(bti.rarc_data, header))?;

            let mut out = vec![];
            let mut cursor = Cursor::new(&mut out);
            conv.write_bti(&mut cursor)?;
            fs::write(&output, &out)?;
        }
        Conversion::BtiToPng => {
            // convert to png, mod header, convert to bti, then back to png
            // todo need a "re-encode new format" method or something
            let png_data = bti.generate_png_data()?;

            let mut png_clean_data = vec![];
            let mut png_cur = Cursor::new(&mut png_clean_data);
            
            // Write the png data as png so it can be read again
            write_buffer_with_format(&mut png_cur, &png_data.1, header.width as u32, header.height as u32, png_data.0, ImageFormat::Png)?;
            
            let data = BufReader::new(Cursor::new(&png_clean_data));
            let comp = BtiCompanion::new(bti.rarc_data, header);
            log::info!("Encoding data to proper format...");
            let conv = Bti::read_png(args.input.clone(), data, comp.clone())?;
            let (image_type, buf) = conv.generate_png_data()?;
            comp.write(&output)?;
            save_buffer_with_format(
                &output,
                &buf,
                conv.header.width as u32,
                conv.header.height as u32,
                image_type,
                ImageFormat::Png,
            )?;
        }
        Conversion::PngEdit => {
            let data = BufReader::new(Cursor::new(&data));
            let comp = BtiCompanion::new(bti.rarc_data, header);
            log::info!("Encoding data to proper format...");
            let conv = Bti::read_png(args.input.clone(), data, comp.clone())?;

            let (image_type, buf) = conv.generate_png_data()?;
            comp.write(&output)?;
            save_buffer_with_format(
                &output,
                &buf,
                conv.header.width as u32,
                conv.header.height as u32,
                image_type,
                ImageFormat::Png,
            )?;
        }
        Conversion::BtiEdit => {
            // convert to png, mod header, convert to bti, then back to png
            // todo need a "re-encode new format" method or something
            let png_data = bti.generate_png_data()?;

            let mut png_clean_data = vec![];
            let mut png_cur = Cursor::new(&mut png_clean_data);
            
            // Write the png data as png so it can be read again
            write_buffer_with_format(&mut png_cur, &png_data.1, header.width as u32, header.height as u32, png_data.0, ImageFormat::Png)?;
            
            let data = BufReader::new(Cursor::new(&png_clean_data));
            let comp = BtiCompanion::new(bti.rarc_data, header);
            log::info!("Encoding data to proper format...");
            let conv = Bti::read_png(args.input.clone(), data, comp.clone())?;

            let mut out = vec![];
            let mut cursor = Cursor::new(&mut out);
            conv.write_bti(&mut cursor)?;
            fs::write(&output, &out)?;
        }
    }

    Ok(())
}

fn set_header(header: &mut Header, args: Args) {

    if let Some(v) = args.min_filter {
        header.min_filter = v;
    }
    if let Some(v) = args.mag_filter {
        header.mag_filter = v.into();
    }
    if let Some(v) = args.format {
        let format = v.into();
        header.format = format;
        header.palette = header.format.use_palette();
        if header.format == TextureFormat::C8 {
            header.palette_count = header.format.num_colors() as u16;
        }
    }
    if let Some(v) = args.palette_format {
        header.palette_format = v;
    }
    if let Some(v) = args.alpha {
        header.alpha = v;
    }
    if let Some(v) = args.wrap_s {
        header.wrap.0 = v;
    }
    if let Some(v) = args.wrap_t {
        header.wrap.1 = v;
    }
    if let Some(v) = args.mipmaps {
        header.mipmap_count = v + 1;
        header.mipmap = v != 0;
    }
    if let Some(v) = args.min_lod {
        header.min_lod = v;
    }
    if let Some(v) = args.max_lod {
        header.max_lod = v;
    }
    if let Some(v) = args.unknown {
        header.unknown = v;
    }
    if let Some(v) = args.edge_lod {
        header.edge_lod = v.is_none_or(|i| i);
    }
    if let Some(v) = args.lod_bias_clamp {
        header.bias_clamp = v.is_none_or(|i| i);
    }
    if let Some(v) = args.lod_bias {
        header.lod_bias = v;
    }
    if let Some(v) = args.anisotropy {
        header.max_anisotropy = v;
    }
    
}
