mod app;
mod cli;
mod error;
mod util;
mod hover;

use std::ffi::OsString;
use std::path::PathBuf;
use std::process::exit;

use clap::Parser;
use clap::error::ErrorKind as ClapErrKind;
use directories::ProjectDirs;
use eframe::egui;
use flexi_logger::{Cleanup, Criterion, Duplicate, FileSpec, Logger, Naming};
use image::ImageFormat;
use lazy_static::lazy_static;

use self::app::MainApp;
use self::cli::{process_conversion, Args};
use self::error::Error;

pub const ICON: &[u8; 4944] = include_bytes!("../resources/btiz.png");
lazy_static!(
    pub static ref ICON_PNG: Vec<u8> = {
        let img = image::load_from_memory_with_format(ICON, ImageFormat::Png).expect("Failed to load icon");
        img.into_rgba8().into_raw()
    };
);

pub static mut OPEN_WITH: Option<PathBuf> = None;

#[cfg(windows)]
fn hide_console_window() {
    use std::ptr;
    use winapi::um::wincon::GetConsoleWindow;
    use winapi::um::winuser::{ShowWindow, SW_HIDE};

    let window = unsafe {GetConsoleWindow()};
    // https://learn.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-showwindow
    if window != ptr::null_mut() {
        unsafe {
            ShowWindow(window, SW_HIDE);
        }
    }
}

fn main() {
    let dirs = ProjectDirs::from("com", "zenny3d", "btiz").unwrap();
    let mut log_dir = dirs.data_local_dir().to_path_buf();
    log_dir.push("logs");
    // Run app
    Logger::try_with_str("info")
        .expect("Failed to setup logger")
        .log_to_file(FileSpec::default().directory(log_dir))
        .rotate(
            Criterion::Size(10 * 1024 * 1024), // Rotate after 10MB
            Naming::Timestamps,
            Cleanup::KeepLogAndCompressedFiles(3, 5), // Keep up to 3 log files and 5 compressed
        )
        .duplicate_to_stderr(Duplicate::All)
        .start()
        .expect("Failed to start logger");

    match Args::try_parse() {
        Ok(args) => {
            match process_conversion(args) {
                Ok(_) => exit(0),
                Err(e) => {
                    log::error!("{}", e);
                    exit(-1);
                }
            }
        }
        Err(e) => {
            // if something is missing, they're probably just running the app
            match e.kind() {
                ClapErrKind::MissingRequiredArgument |
                ClapErrKind::DisplayHelpOnMissingArgumentOrSubcommand => {},
                ClapErrKind::DisplayHelp | ClapErrKind::DisplayVersion => {
                    println!("{}", e);
                    exit(0);
                }
                ClapErrKind::UnknownArgument => {
                    let mut args = std::env::args_os();
                    if args.len() == 2 {
                        let _ = args.next();
                        unsafe {
                            let arg: OsString = args.next().unwrap_unchecked();
                            if arg.to_str().is_some_and(|s| s.ends_with(".png") || s.ends_with(".bti")) {
                                let path = PathBuf::from(arg);
                                if path.exists() {
                                    OPEN_WITH = Some(path);
                                } else {
                                    log::error!("Parse Error: {}", Error::FileNotExist);
                                    exit(-1);
                                }
                            } else {
                                log::error!("Parse Error: {:?}: {}", e.kind(), e.to_string());
                                exit(-1);
                            }
                        }
                    } else {
                        log::error!("Parse Error: {:?}: {}", e.kind(), e.to_string());
                        exit(-1);
                    }
                }
                _ => {
                    log::error!("Parse Error: {:?}: {}", e.kind(), e.to_string());
                    exit(-1);
                }
            }
        }
    }

    // Hide console for app on windows
    #[cfg(windows)]
    {
        hide_console_window();
    }

    let options = eframe::NativeOptions {
        viewport: egui::ViewportBuilder::default()
            .with_inner_size([1152.0, 648.0])
            .with_resizable(false)
            .with_icon(egui::IconData {
                rgba: ICON_PNG.to_vec(),
                width: 128,
                height: 128,
            })
            .with_maximize_button(false),

        ..Default::default()
    };

    eframe::run_native(
        "BTIZ",
        options,
        Box::new(|cc| MainApp::create(cc)),
    )
    .unwrap();
}

