# BTIZ

btiz is an application to convert to and from gamecube bti files and edit bti properties while seeing a visual
representation of what the image will look like. The visual representation is currently only 2D, so properties
that require a 3D view can not be seen.

All property labels offer a tooltip when hovered, which should give a little bit of insight into what each property
does.

## BTI
The generated bti data will be nearly 1:1 due to very minor flaws, but should visually be 1:1. Mipmaps however are
generated in a very simple way and original mipmaps are not retained, which means mipmap data will be different.

## Quantization
This application offers Floyd Steinberg dithering for the C8 texture format's palette generation, allowing for
perceptually pleasing images with a relatively small color palette. Currently no other dithering is supported, however
textures that are around or below 256 colors will not be dithered in order to retain the original data.
